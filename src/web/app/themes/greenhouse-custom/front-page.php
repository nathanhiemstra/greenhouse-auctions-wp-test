<?php

if ( 'posts' == get_option( 'show_on_front' ) ) {
    include( get_home_template() );
}
else {
    if ( ! is_page_template() ) {
        get_header();

        // get_template_part( 'template-parts/front-page/cover' );
        // get_template_part( 'template-parts/front-page/services' );

        $acf_auction_group_1 = get_field('acf_auction_group_1');
        $acf_auction_1_shortcode =  $acf_auction_group_1["acf_auction_shortcode"];
        $acf_auction_1_date =  $acf_auction_group_1["acf_auction_1_date"];
        $acf_auction_1_description =  $acf_auction_group_1["acf_auction_1_description"];
        // print "<br><br>Var Dump: <br>";
        // var_dump($acf_auction_group_1);



        // print "<br><br>acf_auction_group_1_shortcode: <br>";
        // echo  $acf_auction_group_1[''];
       

        ?>
        <div class="container">
            
            <div class="border p-4">

                <h1>Current Auction</h1>
                <p><?=$acf_auction_1_date?></p>
                <p><?=$acf_auction_1_description?></p>
                <div><?=$acf_auction_1_shortcode?></div>
           
            </div>

        </div>

        



        <?php if ( get_theme_mod( 'show_main_content', 1 ) ) : ?>
        <section class="wp-bp-main-content">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <?php while ( have_posts() ) : the_post(); ?>
                            <h2 class="text-center mb-4"><?php the_title(); ?></h2>
                            <?php wp_bootstrap_4_post_thumbnail(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php
        get_footer();
    }
    else {
        include( get_page_template() );
    }
}
?>
